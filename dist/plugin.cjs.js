'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var core = require('@capacitor/core');

const ZebraScanner = core.registerPlugin('ZebraScanner', {
    web: () => Promise.resolve().then(function () { return web; }).then(m => new m.ZebraScannerWeb()),
});

class ZebraScannerWeb extends core.WebPlugin {
    constructor() {
        super({
            name: 'ZebraScanner',
            platforms: ['web'],
        });
    }
    async iniciar() {
        return new Promise((_resolve, reject) => {
            reject('No implementado para web');
        });
    }
}

var web = /*#__PURE__*/Object.freeze({
    __proto__: null,
    ZebraScannerWeb: ZebraScannerWeb
});

exports.ZebraScanner = ZebraScanner;
//# sourceMappingURL=plugin.cjs.js.map
