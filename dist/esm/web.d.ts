import { WebPlugin } from '@capacitor/core';
import { ZebraScannerPluginWeb } from './definitions';
export declare class ZebraScannerWeb extends WebPlugin implements ZebraScannerPluginWeb {
    constructor();
    iniciar(): Promise<{
        value: string;
    }>;
}
