import { PluginListenerHandle } from "@capacitor/core";
export interface ZebraInfoScan {
    tipo: string;
    dato: string;
    cadena: string;
    metodo: string;
}
export interface ZebraScannerPlugin {
    iniciar(): Promise<void>;
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    addListener(eventName: 'scan', listenerFunc: (info: ZebraInfoScan) => void): Promise<PluginListenerHandle> & PluginListenerHandle;
}
export interface ZebraScannerPluginWeb {
    iniciar(): Promise<{
        value: string;
    }>;
}
