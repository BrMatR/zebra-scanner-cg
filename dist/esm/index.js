import { registerPlugin } from '@capacitor/core';
const ZebraScanner = registerPlugin('ZebraScanner', {
    web: () => import('./web').then(m => new m.ZebraScannerWeb()),
});
export * from './definitions';
export { ZebraScanner };
//# sourceMappingURL=index.js.map