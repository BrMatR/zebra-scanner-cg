import type { ZebraScannerPlugin } from './definitions';
declare const ZebraScanner: ZebraScannerPlugin;
export * from './definitions';
export { ZebraScanner };
