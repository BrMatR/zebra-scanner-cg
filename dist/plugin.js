var capacitorZebraScanner = (function (exports, core) {
    'use strict';

    const ZebraScanner = core.registerPlugin('ZebraScanner', {
        web: () => Promise.resolve().then(function () { return web; }).then(m => new m.ZebraScannerWeb()),
    });

    class ZebraScannerWeb extends core.WebPlugin {
        constructor() {
            super({
                name: 'ZebraScanner',
                platforms: ['web'],
            });
        }
        async iniciar() {
            return new Promise((_resolve, reject) => {
                reject('No implementado para web');
            });
        }
    }

    var web = /*#__PURE__*/Object.freeze({
        __proto__: null,
        ZebraScannerWeb: ZebraScannerWeb
    });

    exports.ZebraScanner = ZebraScanner;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

})({}, capacitorExports);
//# sourceMappingURL=plugin.js.map
