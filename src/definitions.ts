import { PluginListenerHandle } from "@capacitor/core";
export interface ZebraInfoScan{
  tipo: string;
  dato: string;
  cadena: string;
  metodo: string;
}
export interface ZebraScannerPlugin {
  iniciar(): Promise<void>;
  echo(options: { value: string }): Promise<{ value: string }>;
  // addListener(eventName: string, listener: (event: any) => void): PluginListenerHandle;
  addListener(
    eventName: 'scan',
    listenerFunc: (info:ZebraInfoScan) => void,
  ): Promise<PluginListenerHandle> & PluginListenerHandle;
  // solicitarPerfiles(): Promise<{ datos: [string] }>;
  // solicitarEstadoPerfil(options: {
  //   nombre: string;
  // }): Promise<{ nombre: string; estado: string }>;
  // activarPerfil(options: {
  //   nombre: string;
  // }): Promise<{ estado: string; nombre: string }>;
  // solicitarPerfilActivo(): Promise<{ nombre: string }>;
}

export interface ZebraScannerPluginWeb {
  iniciar(): Promise<{ value: string }>;
}
