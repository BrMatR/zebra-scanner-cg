import { registerPlugin } from '@capacitor/core';

import type { ZebraScannerPlugin } from './definitions';

const ZebraScanner = registerPlugin<ZebraScannerPlugin>('ZebraScanner', {
  web: () => import('./web').then(m => new m.ZebraScannerWeb()),
});

export * from './definitions';
export { ZebraScanner };
