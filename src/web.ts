import { WebPlugin } from '@capacitor/core';
import { ZebraScannerPluginWeb } from './definitions';


export class ZebraScannerWeb extends WebPlugin implements ZebraScannerPluginWeb {
  constructor() {
    super({
      name: 'ZebraScanner',
      platforms: ['web'],
    });
  }
  async iniciar(): Promise<{ value: string }> {
    return new Promise((_resolve, reject) => {
      reject('No implementado para web');
    });
  }
}