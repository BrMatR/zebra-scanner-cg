# zebra-scanner-cg

Plugin para conectar con el scanner de zebra

## Install

```bash
npm install zebra-scanner-cg
npx cap sync
```

## API

<docgen-index>

* [`iniciar()`](#iniciar)
* [`echo(...)`](#echo)
* [`addListener('scan', ...)`](#addlistenerscan)
* [Interfaces](#interfaces)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### iniciar()

```typescript
iniciar() => Promise<void>
```

--------------------


### echo(...)

```typescript
echo(options: { value: string; }) => Promise<{ value: string; }>
```

| Param         | Type                            |
| ------------- | ------------------------------- |
| **`options`** | <code>{ value: string; }</code> |

**Returns:** <code>Promise&lt;{ value: string; }&gt;</code>

--------------------


### addListener('scan', ...)

```typescript
addListener(eventName: 'scan', listenerFunc: (info: ZebraInfoScan) => void) => Promise<PluginListenerHandle> & PluginListenerHandle
```

| Param              | Type                                                                       |
| ------------------ | -------------------------------------------------------------------------- |
| **`eventName`**    | <code>'scan'</code>                                                        |
| **`listenerFunc`** | <code>(info: <a href="#zebrainfoscan">ZebraInfoScan</a>) =&gt; void</code> |

**Returns:** <code>Promise&lt;<a href="#pluginlistenerhandle">PluginListenerHandle</a>&gt; & <a href="#pluginlistenerhandle">PluginListenerHandle</a></code>

--------------------


### Interfaces


#### PluginListenerHandle

| Prop         | Type                                      |
| ------------ | ----------------------------------------- |
| **`remove`** | <code>() =&gt; Promise&lt;void&gt;</code> |


#### ZebraInfoScan

| Prop         | Type                |
| ------------ | ------------------- |
| **`tipo`**   | <code>string</code> |
| **`dato`**   | <code>string</code> |
| **`cadena`** | <code>string</code> |
| **`metodo`** | <code>string</code> |

</docgen-api>
