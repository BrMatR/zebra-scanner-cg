package com.zebra.scanner.cg;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;

import com.getcapacitor.Bridge;
import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

@CapacitorPlugin(name = "ZebraScanner")
public class ZebraScannerPlugin extends Plugin {
    static final String TIPO = "com.symbol.datawedge.label_type";
    static final String CADENA = "com.symbol.datawedge.data_string";
    static final String DATO = "com.motorolasolutions.emdk.datawedge.decode_data";
    static final String METODO = "com.symbol.datawedge.decoded_mode";
    static final String ACTION = "com.symbol.datawedge.api.ACTION";
    static final String EXTRAS_GET_PROFILES = "com.symbol.datawedge.api.GET_PROFILES_LIST";
    static final String RESULT_GET_PROFILES = "com.symbol.datawedge.api.RESULT_GET_PROFILES_LIST";
    static final String RESULT_GET_CONFIG = "com.symbol.datawedge.api.RESULT_GET_CONFIG";

    static final String RESULT_ACTION = "com.symbol.datawedge.api.RESULT_ACTION";
    static final String EXTRAS_GET_CONFIG = "com.symbol.datawedge.api.GET_CONFIG";

    Intent i = new Intent();
    Plugin context;
    PluginCall call;
    //i.setAction("com.symbol.datawedge.api.ACTION");
    //i.putExtra("com.symbol.datawedge.api.GET_CONFIG", "<profile name>");
@PluginMethod()
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.resolve(ret);
    }
    @PluginMethod()
    public void iniciar(PluginCall call) {
        IntentFilter filter = new IntentFilter();
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        filter.addAction(ZebraScannerPlugin.RESULT_ACTION);
        filter.addAction("ar.com.compragamer.lector.deposito.IO");
        this.context = getPluginHandle().getInstance();
        this.context.getActivity().registerReceiver(pluginBroadcastReceiver, filter);
    }
 
    // @PluginMethod()
    // public void solicitarEstadoPerfil(PluginCall call) {
    //     String perfil = call.getString("nombre");
    //     Log.d("PERFIL", perfil);
    //     Intent intentProfiles = new Intent();
    //     Bundle bMain = new Bundle();
    //     bMain.putString("PROFILE_NAME", perfil);
    //     intentProfiles.setAction(ZebraScannerPlugin.ACTION);
    //     intentProfiles.putExtra(ZebraScannerPlugin.EXTRAS_GET_CONFIG, bMain);
    //     context.getActivity().sendBroadcast(intentProfiles);
    // }

    // @PluginMethod()
    // public void solicitarPerfilActivo(PluginCall call) {
    //     call.setKeepAlive(true);
    //     Intent i = new Intent();
    //     i.setAction("com.symbol.datawedge.api.ACTION");
    //     i.putExtra("com.symbol.datawedge.api.GET_ACTIVE_PROFILE", "");
    //     context.getActivity().sendBroadcast(i);
    // }

    // @PluginMethod()
    // public void activarPerfil(PluginCall call) {
    //     String perfil = call.getString("nombre");
    //     call.setKeepAlive(true);
    //     Intent i2 = new Intent();
    //     i2.setAction("com.symbol.datawedge.api.ACTION");
    //     i2.putExtra("com.symbol.datawedge.api.SWITCH_TO_PROFILE", perfil);
    //     i2.putExtra("SEND_RESULT", "true");
    //     i2.putExtra("COMMAND_IDENTIFIER", "123456789");
    //     context.getActivity().sendBroadcast(i2);
    //     Log.d("--PERFIL para activar", perfil);
    // }


    // @PluginMethod()
    // public void solicitarPerfiles(PluginCall call) {
    //     call.setKeepAlive(true);
    //     Intent intentProfiles = new Intent();
    //     intentProfiles.setAction(ZebraScannerPlugin.ACTION);
    //     intentProfiles.putExtra(ZebraScannerPlugin.EXTRAS_GET_PROFILES, "");
    //     context.getActivity().sendBroadcast(intentProfiles);
    //     //startActivityForResult(this.call, intentProfiles,123);// (this.call,intentProfiles,123);

    // }

    private BroadcastReceiver pluginBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle b = intent.getExtras();
            Log.v("ACTION --G--", action);
            for (String key : b.keySet()) {
                Log.v(key + "--G--", b.get(key).toString());
            }
            switch (action) {
                case "ar.com.compragamer.lector.deposito.IO":
                    JSObject ret = new JSObject();
                    ret.put("tipo", b.get(ZebraScannerPlugin.TIPO));
                    ArrayList f = (ArrayList) b.get(ZebraScannerPlugin.DATO);
                    byte[] codigo = ((byte[]) f.get(0));
                    String ss = new String(codigo);
                    ret.put("dato", ss);
                    ret.put("cadena", b.get(ZebraScannerPlugin.CADENA));
                    ret.put("metodo", b.get(ZebraScannerPlugin.METODO));

                    notifyListeners("scan", ret);
                    break;
                // case ZebraScannerPlugin.RESULT_ACTION:


                //     if (intent.hasExtra("RESULT_INFO")) {

                //         String command = intent.getStringExtra("COMMAND");

                //         if (command.equals("com.symbol.datawedge.api.SWITCH_TO_PROFILE")) {
                //             String commandidentifier = intent.getStringExtra("COMMAND_IDENTIFIER");
                //             String result = intent.getStringExtra("RESULT");
                //             Bundle bundle2 = new Bundle();
                //             String resultInfo = "";
                //             bundle2 = intent.getBundleExtra("RESULT_INFO");
                //             String nombrePerfil = bundle2.getString("PROFILE_NAME");
                //             String codigoRespuesta = bundle2.getString("RESULT_CODE");
                //             JSObject respuesta = new JSObject();
                //             respuesta.put("nombre", nombrePerfil);
                //             respuesta.put("estado", codigoRespuesta == null || codigoRespuesta.equals("PROFILE_ALREADY_SET"));
                //             notifyListeners("activarPerfil", respuesta);
                //         }
                //     }
                //     if (intent.hasExtra(ZebraScannerPlugin.RESULT_GET_CONFIG)) {
                //         Bundle bundle = intent.getBundleExtra(ZebraScannerPlugin.RESULT_GET_CONFIG);
                //         if (bundle != null && !bundle.isEmpty()) {
                //             Set<String> keys = bundle.keySet();
                //             if (keys.contains("RESULT_CODE")) {
                //                 Log.d("codigo<", bundle.getString("RESULT_CODE"));
                //                 return;
                //             }
                //             String profileName = bundle.getString("PROFILE_NAME");
                //             String profileEnabled = bundle.getString("PROFILE_ENABLED");
                //             //Log.d(profileName,profileEnabled);
                //             JSObject respuesta = new JSObject();
                //             respuesta.put("nombre", profileName);
                //             respuesta.put("estado", profileEnabled);
                //             notifyListeners("estadoPerfil", respuesta);
                //         }
                //     }

                //     if (intent.hasExtra(ZebraScannerPlugin.RESULT_GET_PROFILES)) {
                //         String[] profilesList = intent.getStringArrayExtra("com.symbol.datawedge.api.RESULT_GET_PROFILES_LIST");
                //         try {
                //             JSArray listaJS = new JSArray(profilesList);
                //             //for(String profile: profilesList ){
                //             //    solicitarEstadoPerfil(perfil);
                //             //}
                //             JSObject respuesta = new JSObject();
                //             respuesta.put("datos", new JSONArray(profilesList));
                //             Log.d("TAG", Arrays.toString(profilesList));
                //             notifyListeners("listaPerfiles", respuesta);
                //         } catch (JSONException e) {
                //             e.printStackTrace();
                //         }
                //     }

                //     if (intent.hasExtra("com.symbol.datawedge.api.RESULT_GET_ACTIVE_PROFILE")) {
                //         String activeProfile = intent.getStringExtra("com.symbol.datawedge.api.RESULT_GET_ACTIVE_PROFILE");
                //         JSObject respuesta = new JSObject();
                //         respuesta.put("nombre", activeProfile);
                //         Log.d("Perfil Activo", activeProfile);
                //         notifyListeners("perfilActivo", respuesta);
                //     }


                //     break;
                default:
            }
        }
    };
}
